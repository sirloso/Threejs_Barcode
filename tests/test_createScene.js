let THREE = require('three');
let idx = require('../index')

let {scene,camera,code} = idx.createScene('hello')
scene.background = new THREE.Color('white')
var renderer = new THREE.WebGLRenderer();
renderer.setSize(600, 150);
document.body.appendChild(renderer.domElement);

camera.position.x = 1.675;
camera.position.z = 5.75;

function animate(){
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
    code.animate()
}
animate()
console.log(scene);