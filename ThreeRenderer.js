let THREE = require('three');
const { Group } = require('three');

const noop = () => { };

global.addEventListener = noop;
global.HTMLImageElement = noop;
global.HTMLCanvasElement = noop;

class ThreeRenderer {
    constructor(data,opts={ w:0.1,h:1, padding: -40 }) {
        this.data = data
        this.opts = opts
    }

    group(...objs){
        var group = new THREE.Group();
        for (let obj of objs) group.add(obj)
        return group
    }

    render() {
        let binary = this.data 

        // shape used for units in bar graph
        var geometry = new THREE.BoxBufferGeometry(this.opts.w,this.opts.h,1);

        // colors 
        var materialW = new THREE.MeshBasicMaterial( { color: "white" } );
        var materialB = new THREE.MeshBasicMaterial( { color: "black" } );

        var bars = []
        // construct code
        for( let c of binary ){
            var bar
            if( c === '0' ){
                //white
                bar = new THREE.Mesh( geometry, materialW );
            }else{
                //black 
                bar = new THREE.Mesh( geometry, materialB );
            }
            bar.position.set(this.opts.padding*this.opts.w,0,0)
            this.opts.padding+=1
            bars.push(bar)
        }

        this.codeGroup = this.group(...bars)

        return this.codeGroup 
    }

    setAnimate(fna) {
        this._animate = fna 
        this._animate = this._animate.bind(this)
    }

    animate(){
        if(this._animate) this._animate()
        else{
            console.log('error no animate function registered');
        }
    }
}

module.exports = { ThreeRenderer }