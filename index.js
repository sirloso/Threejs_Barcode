let ThreeRenderer = require('./ThreeRenderer.js')
let jsb  = require("jsbarcode")
let THREE = require('three');

const createBarcodeObject = (word, opts = { w: 0.05, h:4, padding: -40 } ) => {
    let data = {}
    jsb(data,word)
    if( !opts.w && !opts.h && !opts.padding ) throw new Error('missing one or more require fields (w,h,padding) ')
    let code = new ThreeRenderer.ThreeRenderer(data.encodings[0].data,{ ...opts })
    return code 
} 

const createRenderer = (elementId,width=600,height=150) => {
    //renderer
    var renderer = new THREE.WebGLRenderer();
    renderer.setSize(width, height);
    document.getElementById(elementId).appendChild(renderer.domElement);
    return renderer
}

const animateFn = (renderer,scene,camera,...animations) => {
        return function animate(renderer,scene,camera,...animations){
            requestAnimationFrame(animate);
            renderer.render(scene, camera);
            for( let anfn of animations ) try{ anfn.animate() } catch(e){ console.log(e) }
    }
}

const createScene = (word,opts = { w: 0.05, h:4, padding: -40 } ) => {
let data = {}
jsb(data,word) 
let code = new ThreeRenderer.ThreeRenderer(data.encodings[0].data,{...opts})

// start setup
var scene = new THREE.Scene();
// scene.background = new THREE.Color( 'white' );
var camera = new THREE.PerspectiveCamera(52, 1.5, 1, 1000);


r = code.render()

scene.add(r);

// camera.position.x = 1.675;
// camera.position.z = 5.75;

    return {scene,camera,barcode: code}
}

module.exports = {
    createScene,
    createBarcodeObject,
    ThreeRenderer,
    THREE,
    animateFn,
    createRenderer
}